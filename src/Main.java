import org.uu.nl.net2apl.core.agent.Agent;
import org.uu.nl.net2apl.core.agent.AgentCreationFailedException;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.fipa.FIPAMessenger;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.fipa.mts.Envelope;
import org.uu.nl.net2apl.core.messaging.Messenger;
import org.uu.nl.net2apl.core.platform.Platform;

import java.net.URISyntaxException;

public class Main {
    
    public static void main(String[] args) {

        /*
          In order to create a platform, we first need a messenger-object. In this example, we need a messenger that
          can handle messages that can be (de)serialized, since the platforms may be on different machines.
          (Note: This might just become the standard in the future, in which case you'd just create the platform.)
        */
        Messenger<?> messenger = new FIPAMessenger();

        /*
          For this simple application, a single platform will be created. The platform is where agents can find each other,
          and service as the basic environment.
         */
        Platform platform = Platform.newPlatform(2, messenger);


        /*
          Create a yellow-pages agent. It's assumed that, in the case where Agents rely on each other as services
          (instead of being given the exact ID of other Agents on startup). This will most often be the case when the
          system can have different configurations (more than one kind of a certain service for instance),
          or is distributed.
         */
        try {
            Agent yellowPages = platform.newDirectoryFacilitator();
            
            /*
              We'll print out the ID of the new DF here, so it can be used to 'feed' the next program/platform.
             */
            System.out.println("Created DirectoryFacilitator, AgentID:");
            System.out.println(yellowPages.getAID().toString());
            
        } catch (URISyntaxException | AgentCreationFailedException ex) {
            System.err.println("Failed to create yellow-pages (= directory facilitator) Agent.");
            System.exit(1);
        }
        
        /*
          Create two 'HelloWorld' Agents.
          A hello world agent will register as a hello world service (via the yellow-pages),
          then ask (that same/the) yellow-pages whether there are any other hello-world 'services' around,
          lastly initiate a
         */
        try {
            Agent helloA = new HelloWorldAgent(platform);
            Agent helloB = new HelloWorldAgent(platform);
            Agent helloC = new HelloWorldAgent(platform);
        } catch (URISyntaxException ex) {
            System.err.println("Failed to create a Hello World Agent.");
            System.exit(1);
        }
        
        /*
          Note that the program does not exit, as the Agents are still active (albeit not doing anything after the
          initial messages, since no new hello-world agents are created.)
         */
    }

    /**
     * If a lot of fields are specified, sending a message can get quite verbose, so it's advisable to write a
     * separate method if a lot of similar looking mesasges need to be sent.
     *
     * Of course, usually this method should not be in the main class
     *
     * @param aidMe             AgentID of agent sending the message
     * @param aidThem           AgentID of intended receiver
     * @param performative      Performative to use for the message
     * @param content           Actual message content
     * @return                  ACL message that can be sent by the agent
     */
    static ACLMessage createMessage(AgentID aidMe, AgentID aidThem, Performative performative, String content) {
        Envelope envelope = new Envelope();
        envelope.setFrom(aidMe);
        envelope.addTo(aidThem);
        envelope.addIntendedReceiver(aidThem);

        ACLMessage message = new ACLMessage(performative);
        message.addReceiver(aidThem);
        message.addReplyTo(aidMe);
        message.setSender(aidMe);
        message.setContent(content);
        message.setEnvelope(envelope);

        return message;
    }
}			
