import org.uu.nl.net2apl.core.agent.*;
import org.uu.nl.net2apl.core.defaults.messenger.MessageReceiverNotFoundException;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.fipa.ams.DirectoryFacilitator;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;
import org.uu.nl.net2apl.core.plan.builtin.FunctionalPlanSchemeInterface;
import org.uu.nl.net2apl.core.plan.builtin.SubPlanInterface;
import org.uu.nl.net2apl.core.platform.PlatformNotFoundException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

class DFRegistrationArguments extends AgentArguments {

    DFRegistrationArguments(String providesService, String... subscribesToService) {
        super();

        super.addContext(new DFRegistrationContext(providesService, subscribesToService));
        super.addMessagePlanScheme(new RegistrationMessagePlanScheme());
        super.addInitialPlan(new RegisterWithDFSubPlanInterface());
    }

    static class RegistrationMessagePlanScheme implements FunctionalPlanSchemeInterface {

        /**
         * Get the plan for a given trigger and context interface. Return SubPlanInterface.UNINSTANTIATED if the plan did not fire.
         *
         * @param trigger
         * @param contextInterface
         */
        @Override
        public SubPlanInterface getPlan(Trigger trigger, AgentContextInterface contextInterface) {

            if(trigger instanceof ACLMessage) {
                ACLMessage received = (ACLMessage) trigger;

                switch (received.getPerformative()) {
                    /*
                      When an agent is created, all existing Directory Facilitator agents will sent it a message with the
                      PROPOSE performative. This is a proposal to register with the DF.
                     */
                    case PROPOSE:
                        return new RegisterWithDFSubPlanInterface(received.getSender());
                    /*
                      After registration, the DF will reply with an INFORM message, with the content a list of other agent's
                      that provide the service this agent subscribed to. If other agents are added later with this service,
                      the INFORM message will be received again, with those agent ID's as well.
                     */
                    case INFORM:
                        return (planInterface -> {
                            DFRegistrationContext context = planInterface.getContext(DFRegistrationContext.class);
                            String[] messageContentParts = received.getContent().split(" ");

                            /*
                              The DF sends a message with the performative INFORM, and a list of agents providing one
                              of the requested services as the content. The string should be split by spaces, where the
                              first word is the name of the provided service, and the other words are agent ID's of
                              agents providing that service
                             */
                            Arrays.stream(messageContentParts).skip(1)
                                    .filter(aid -> !aid.equals(planInterface.getAgentID().toString()))
                                    .forEach(aid -> {
                                        try {
                                            /*
                                              Message content is always a string. To send a message to the agents, the
                                              string first has to be converted to an AgentID again
                                             */
                                            AgentID agentID = new AgentID(new URI(aid));

                                            /*
                                              The first item in the content parts is the service name, all consecutive
                                              are agentID's. The current agent adds the received agents to its belief
                                              (Context), so it can request services of that agent.

                                              Note that we filter out the agent ID of the receiving agent, because in
                                              this case it provides the same service as it subscribes to
                                             */
                                            context.addSubscription(messageContentParts[0], agentID);

                                            System.out.println(planInterface.getAgentID() + " subscribed to service " +
                                                    messageContentParts[0] + " of agent " + aid);

                                            /*
                                              In this case, the agent requests the service right away. Of course, that
                                              is not always the case, but for this example, it is the easiest way to
                                              trigger the agent behavior.
                                             */
                                            ACLMessage message = Main.createMessage(
                                                    planInterface.getAgentID(),
                                                    agentID,
                                                    Performative.REQUEST,
                                                    "Hello, " + agentID.getShortLocalName());
                                            planInterface.getAgent().sendMessage(message);
                                        } catch (URISyntaxException | MessageReceiverNotFoundException | PlatformNotFoundException e) {
                                            e.printStackTrace();
                                            System.err.println("Could not convert " + aid
                                                    + " to AgentID. No subscription added and the service was not requested!");
                                        }
                            });

                        });

                    default:
                        return SubPlanInterface.UNINSTANTIATED;
                }
            }

            return SubPlanInterface.UNINSTANTIATED;
        }
    }

    /**
     * A plan to register with one or more (specified) DF's.
     *
     * Note that Plan is extended, as well as the interface <code>SubPlanInterface</code> is implemented. Both require
     * the single method <code>execute(...)</code> so this works in this instance. The reason this is done, is because a
     * SubPlanInterface cannot be added as an initial plan, but a <code>Plan</code> cannot be used in the
     * <code>FunctionalPlanSchemeInterface</code> above.
     */
    static class RegisterWithDFSubPlanInterface extends Plan implements SubPlanInterface {

        private Set<AgentID> dfsToContact;

        /**
         * Construct an instance of this class without specifying any DF's to contact. In that case, it is assumed all
         * available DF's should be contacted. The reason is that this most likely means the plan was adopted as an
         * InitialPlan in the agent arguments, where the list of DF's cannot be queried.
         */
        RegisterWithDFSubPlanInterface() {
            this.dfsToContact = new HashSet<>();
        }

        /**
         * Construct an instance of this class with a specification of which DF's to contact. Only the DF's listed as
         * the argument here will be contacted for the execution of this plan.
         * @param dfsToContact
         */
        RegisterWithDFSubPlanInterface(AgentID ... dfsToContact) {
            this.dfsToContact = Set.of(dfsToContact);
        }

        /**
         * Specification of the plan to be executed.
         *
         * Note that when using PlanSchemes, instead of a functional planscheme interface, a Plan is expected, in which
         * case the plan needs to be marked as finished using <code>setFinished(true)</code>. This is not necessary
         * here as a subplan is only executed once.
         *
         * @param planInterface     Interface to the plan
         */
        @Override
        public void execute(PlanToAgentInterface planInterface) throws PlanExecutionError {
            DFRegistrationContext context = planInterface.getContext(DFRegistrationContext.class);

            if(context == null) throw new PlanExecutionError();

            if(this.dfsToContact.size() == 0) {
                try {
                    this.dfsToContact = planInterface.getAgent().getPlatform().getLocalDirectoryFacilitators();
                } catch (PlatformNotFoundException e) {
                    e.printStackTrace();
                    System.exit(5);
                }
            }

            for (AgentID aid: this.dfsToContact) {
                /*
                  First a check to see if this agent already registered with the given DF. No need to register twice.
                 */
                if(!context.hasContactedDF(aid)) {
                    try {
                        /*
                          For each DF a few messages need to be sent, but only the content differs. First generate the
                          contents, then send a message for each of those strings to this DF.
                         */
                        for (String messageContent : prepareAllMessages(planInterface, context)) {
                            ACLMessage msg = Main.createMessage(planInterface.getAgentID(), aid, Performative.SUBSCRIBE, messageContent);
                            planInterface.getAgent().sendMessage(msg);
                        }

                        /*
                          Don't forget to 'remember' this DF has now been contacted.
                         */
                        context.setContactedDF(aid);
                    } catch(MessageReceiverNotFoundException | PlatformNotFoundException e) {
                        e.printStackTrace();
                        System.exit(3);
                    }
                }
            }
        }

        /**
         * Zero or more messages need to be sent by this agent. One for the service this agent provides (if any), and one
         * for each service it subscribes to (if any). These messages may be sent to multiple DF's, if multiple exist
         * on the system, but the messages themselves will not differ from one DF to the next.
         *
         * @param planInterface     PlanToAgentInterface giving the plan access to the agent state
         * @param context           DFRegistrationContext containing agent's belief base about registration state
         * @return                  List of messages to be sent to each of the active DF's
         */
        private List<String> prepareAllMessages(PlanToAgentInterface planInterface, DFRegistrationContext context) {
            List<String> messages = new ArrayList<>();

            /*
              First, the message declaring the service this agent provides
             */
            messages.add(String.format(
                    "%s %s %s",
                    DirectoryFacilitator.RequestType.SERVICE_ADD,
                    planInterface.getAgentID(),
                    context.providesService
            ));

            /*
              One message for each service this agent subscribes to
             */
            context.subscribesToService.forEach(service -> {
                messages.add(String.format(
                        "%s %s %s",
                        DirectoryFacilitator.RequestType.SUBSCRIBER_ADD,
                        planInterface.getAgentID(),
                        service
                ));
            });

            return messages;
        }
    }

    /**
     * This context serves as the belief base of the agent regarding its registration. It keeps track of which
     * DF's it already has registered with, and of all agents that provide a certain service this agent is interested in
     * (i.e. subscribes to).
     */
    static class DFRegistrationContext implements Context {
        String providesService;
        List<String> subscribesToService = new ArrayList<>();

        /** List of DF's already contacted **/
        Set<AgentID> contactedDirectoryFacilitators = new HashSet<>();

        /** Map of service names and the agents providing those services. These should be services the current agent
         * subscribes to **/
        Map<String, Set<AgentID>> subscriptions = new HashMap<>();

        DFRegistrationContext(String providesService, String... subscribesToService) {
            this.providesService = providesService;
            this.subscribesToService.addAll(Arrays.asList(subscribesToService));
        }

        /**
         * Store the AgentID of the DF that has been contacted
         * @param contactedDF
         */
        void setContactedDF(AgentID contactedDF) {
            this.contactedDirectoryFacilitators.add(contactedDF);
        }

        /**
         * Check if this DF has already been contacted by the agent that has this context
         * @param df    AgentID of the DF
         * @return      True iff this DF has already been contacted by the owner of this context
         */
        boolean hasContactedDF(AgentID df) {
            return this.contactedDirectoryFacilitators.contains(df);
        }

        /**
         * Store an agent that provides a specific service for later use
         *
         * @param serviceName   Name of the service the agent provides
         * @param agentID       AgentID of the agent that provides that service
         */
        void addSubscription(String serviceName, AgentID agentID) {
            if(!this.subscriptions.containsKey(serviceName))
                this.subscriptions.put(serviceName, new HashSet<>());

            this.subscriptions.get(serviceName).add(agentID);
        }

        /**
         * Get all agents that are known and which provide a specific service
         * @param serviceName   Service the agents should provide
         * @return Set of AgentID's that provide <code>serviceName</code>
         */
        Set<AgentID> getSubscriptions(String serviceName) {
            return this.subscriptions.get(serviceName);
        }
    }
}
