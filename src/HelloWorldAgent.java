import org.uu.nl.net2apl.core.agent.*;
import org.uu.nl.net2apl.core.defaults.messenger.MessageReceiverNotFoundException;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.plan.builtin.FunctionalPlanSchemeInterface;
import org.uu.nl.net2apl.core.plan.builtin.SubPlanInterface;
import org.uu.nl.net2apl.core.platform.Platform;
import org.uu.nl.net2apl.core.platform.PlatformNotFoundException;

import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The <code>HelloWorldAgent</code>-class is a convenience-class, holding together the
 * <code>(HelloWorld)AgentArguments</code> and <code>(HelloWorld)Context</code>.
 *
 * We could also have created a new Agent by performing something like:
 * <code>Agent a = new Agent(platform, new XYZAgentArguments());</code>
 */
class HelloWorldAgent extends Agent {

    private static final String SERVICE_NAME = "HelloWorld";
    
    HelloWorldAgent(Platform p) throws URISyntaxException {
        super(p, new HelloWorldAgentArguments());
    }
    
    /**
     * A Context can be thought of as a set of knowledge an agent has about certain things.
     * Taken together, all Contexts an Agents has, should represent everything it knows (and occasionally supporting
     * methods for dealing elegantly with this knowledge, or even capabilities, so things an agent may need to do at
     * some point).
     *
     * This could have been handled in the agent(s) itself, but this allows for greater/easier modularity when building
     * complex Agents with multiple contexts.
     */
    static class HelloWorldContext implements Context {
        
        private Set<AgentID> alreadyContacted = new HashSet<>();
        private int replyCount = 0;

        /**
         * Find all agents within a set of agents that have not been contacted yet
         * @param subscribers   Set of agents to check if they have been contacted
         * @return              Set containing only those agents from <code>subscribers</code> that have not yet been
         *                      contacted
         */
        public Set getNewContacts(Collection<AgentID> subscribers) {
            return subscribers.stream().filter(aid -> !alreadyContacted.contains(aid)).collect(Collectors.toSet());
        }

        /**
         * Add the belief that an agent has been contacted to the believe base of the agent that owns this context
         * @param agentID       AgentID of agent that has been contacted
         */
        public void markAgentAsContacted(AgentID agentID) {
            this.alreadyContacted.add(agentID);
        }
        
        public int nextReplyCount() {
            return ++replyCount;
        }
    }


    static class HelloWorldPlan implements FunctionalPlanSchemeInterface {
        
        /*
          The vast majority of plans any Agents will actually perform (other than the startup- and shutdown-plans) are
          precipitated by messages, mostly from other Agents.

          Not shown here: plans triggered by goals or external tasks.

          An external task is the only way something from outside the MAS (e.g. a human user) should interact with an agent.

          A goal can be adopted by the agent itself and should trigger a special kind of plan, which remains associated
          with that goal. A goal can be marked as achieved, and a plan can be marked as finished. If a plan is finished
          but the goal is not achieved, the SubPlanInterface will be used to find a new plan for the goal. This is useful
          for goals that may not be achievable right away, but will be at some future point.
         */
        public SubPlanInterface getPlan(final Trigger trigger, final AgentContextInterface contextInterface) {

            /*
              All plans are triggered by a trigger. In this case, it's a message. Based on some criteria (in this case
              the Performative of the message, but class matching or even complicated belief-lookups on one or more
              Contexts are all allowed.
             */
            if (trigger instanceof ACLMessage) {
                ACLMessage received = (ACLMessage) trigger;

                switch (received.getPerformative()) {
                    default:
                        return SubPlanInterface.UNINSTANTIATED;
                    case REQUEST:
                        /*
                          As you may notice, we return a function/closure here, instead of performing the action 'ourselves'.
                          All of the closures from all Agents are gathered for the current time-step, and only then are
                          they executed.
                         */
                        return (planInterface) -> {

                            /*
                              Since we're inside the closure here, constructed in a static method, we need the planInterface
                              to get most relevant information. Please note that the Nickname of an Agent doesn't count for
                              equality tests between AgentID's.
                             */
                            String myName = planInterface.getAgentID().getShortLocalName();

                            /* A lot of other necessary data is probably from the message that has just been received:
                             */
                            String theirName = received.getSender().getShortLocalName();
                            String greeting = received.getContent();

                            /*
                              Often, some bit of info has to be recollected from, or remembered to, a context.
                              In this case, both (via a convenience method in the relevant context itself.)
                             */
                            int replyCount = planInterface.getContext(HelloWorldContext.class).nextReplyCount();

                            System.out.println("I'm " + myName + " and " + theirName +
                                    " requested the HelloWorld service from me with the greeting  '" + greeting + "'!");

                            try {
                                planInterface.getAgent().sendMessage(Main.createMessage(
                                        planInterface.getAgentID(), received.getSender(), Performative.AGREE,
                                        "I've received your message. You're the " + replyCount +
                                                "-st/nd/th HelloWorldAgent I've replied to!"));
                            } catch (MessageReceiverNotFoundException | PlatformNotFoundException e) {
                                System.err.println("Can't send message.");
                                System.exit(1);
                            }
                        };

                    /*
                      Confirmation that the other Agent got our message, and has responded.
                     */
                    case AGREE:
                        /*
                          In a similar vein to what happened with the 'REQUEST' Performative:
                         */
                        return (planInterface) -> {
                            String myName = planInterface.getAgentID().getShortLocalName();
                            String theirName = received.getSender().getShortLocalName();
                            String greeting = received.getContent();

                            System.out.println("I'm " + myName + " and " + theirName +
                                    " just replied to me with the message '" + greeting + "'!");
                        };
                }
            }

            /*
              SubPanInterface.UNINSTANTIATED is a placeholder that lets the caller know no plan will need to be run in
              this cycle for this Agent.
             */
            return SubPlanInterface.UNINSTANTIATED;
        }
    }

    /**
     * The <code>Arguments</code> subclass is really the glue that hangs a specific kind of Agent together.
     * Where the <code>Agent</code> class can be seen as instantiating a generic agent, <code>AgentArguments</code> is
     * everything that makes an agent a specific one.
     *
     * This could have been handled by inheritance, but the current method was chosen to facilitate easier composition,
     * though this may change in the future as it makes the use of it less transparent to new users of the platform.
     *
     * You may notice that we inherit from <code>DFRegistrationArguments</code> rather than <code>AgentArguments</code>.
     * <code>DFRegistrationArguments</code> is a convenience-class made to facilitate offering/subscribing to a
     * Service easier.
     */
    static class HelloWorldAgentArguments extends DFRegistrationArguments {

        HelloWorldAgentArguments() {
            super(SERVICE_NAME, SERVICE_NAME);

            super.addContext(new HelloWorldContext());
            
            /* We'd add any initial plans an Agent might have with 'super.addInitialPlan(...);' here.
             * There is a similar method for shut-down-plans.
             */
            super.addMessagePlanScheme(new HelloWorldPlan());
        }
    }   
}
