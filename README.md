# Object-Oriented 2APL Demo: Hello World

This application is a demonstration of how to use various triggers to affect agent behavior. In this demo, first the Platform, in which all agents reside, is constructed along with a Directory Facilitator (DF) / Yellow Pages agent. Next, three instances of the `Hello World` agent are constructed, which will find each other through the Directory Facilitator and greet each other. All agent behavior is encapsulated in `Plans`, which are triggered using `Triggers` through a `PlanScheme`.

## Getting Started
First, clone the code. A submodule is added which refers to the 2APL source code, although it can be managed separately as well.
```shell
git clone git clone https://bitbucket.org/goldenagents/oo2apl-hello-world-demo.git 
cd oo2apl-hello-world-demo
git submodule init
git submodule update
```
Next, open the IDE of your choice, and mark the `src` directory as the source directory for your project. Both this demo and 2APL are tested in Java 11, and might not work on older platforms. Add the `net2apl` directory to your sources or libraries in such a way that it will be available to your Java Classpath.

Run `src/Main.java`

# Understanding the code
The code in this demo is heavily documented. It is adviced to start scanning through the `Main.java` class, as this is where the system is initialized and agents are created.

## Agent
The `HelloWorld` agent for this demo itself is specified in `HelloWorldAgent.java`, which is the next class to look at. Note that this agent has a `Context`, which serves as (one of) the belief base(s) of this agent and a `FunctionalPlanSchemeInterface` called `HelloWorldPlan` here, which matches triggers to actual executable plans.

### Plan Schemes 
In this demo, only message triggers are used, which is seen by the fact that the functional plan scheme is added as a `MessagePlanScheme` in the `HelloWorldAgentArguments` at the bottom of the class. Plan scheme bases for external triggers (environment changes or user interaction), goals (adopted by the agent itself which can be pursued over longer periods of time) and internal triggers (used for failed plan recovery) also exist. A seperate plan scheme is needed for each type of trigger, but multiple plan schemes for the same type of trigger are allowed. If `UNINSTANTIATED` is returned by a plan scheme, the agent will keep trying to match the trigger with other applicable plan schemes, until either a plan is found matching the trigger, or all plan schemes have been exhausted.

In this demo, all agent logic is placed in a single class. However, each trigger, plan and plan scheme can be set up as separate classes. Instead of the `SubPlanInterface`, a `Plan` can be used. In that case, the plan scheme should extend `PlanScheme` instead of `FunctionalPlanSchemeInterface`. These types of plans have the advantage that they can also be adopted at any stage by the agent itself, for example as initial plans, instead of only through triggers.

## (De)registration in an open agent environment
The arguments are used to construct the agent and give it its basic plan schemes and believes. The `HelloWorldAgentArgument` in `HelloWorldAgent.java` extend the `DFRegistrationArguments` instead of the `AgentArguments` class of OO2APl itself. This is because registration itself requires some plans and beliefs.

In this demo, the `DFRegistrationArguments` class has been given as an example. The DF is provided by the platform, and agents are expected to register with it, declare services they can provide and subscribe to services they are interested to. In this demo, the `HelloWorld` agents declare the service `helloworld`, which allows them to send a greeting.

As an initial plan, the agent finds the DF's on the platform, and sends a message to these DF's with the service it provides and a message for each service it wants to subscribe to. The DF responds to this last message with a list of agents providing that service and when a new agent providing that service subscribes, will send an update with the ID of that agent as well.

In an open environment, it is up to the agents to declare their own services and subscribe to services properly. Since the agents are autonomous, and any agent can declare any service, agents should for themselves decide which other agents they want to request certain services from. It is also important, although not shown in this demo, to deregister an agent with the same DF if the agent is leaving the open agent environment.

In this demo, a request for the provided service is sent directly to all agents given by the DF after the agent subscribes to a service, although in practical applications, the agents should determine when a service is required themselves.

